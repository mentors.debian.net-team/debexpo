# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2023-09-05 11:51+0000\n"
"Last-Translator: bittin1ddc447d824349b2 <bittin@reimu.nl>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/debexpo/nntp/sv/>"
"\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: nntp/models.py
msgid "Namespace"
msgstr "Namnrymd"

#: nntp/models.py
msgid "List name"
msgstr "Listnamn"

#: nntp/models.py
msgid "Last message processed"
msgstr "Senaste meddelandet behandlat"
